#!/bin/bash
<< COMMENT
Modified version found here: https://docs.microsoft.com/en-us/azure/developer/terraform/store-state-in-azure-storage#configure-storage-account
COMMENT

RESOURCE_GROUP_NAME="achdwick-tfstate-storage-rg"
STORAGE_ACCOUNT_NAME="actfstatestorage$RANDOM" #between 3-24 char, alphanumeric only
CONTAINER_NAME="tfstate"
LOCATION=uksouth

# Create resource group
az group create --name $RESOURCE_GROUP_NAME --location $LOCATION

# Create storage account
az storage account create --resource-group $RESOURCE_GROUP_NAME --name $STORAGE_ACCOUNT_NAME --sku Standard_LRS --encryption-services blob

# Get and export storage account key
export ARM_ACCESS_KEY=$(az storage account keys list --resource-group $RESOURCE_GROUP_NAME --account-name $STORAGE_ACCOUNT_NAME --query '[0].value' -o tsv)

# Create blob container
az storage container create --name $CONTAINER_NAME --account-name $STORAGE_ACCOUNT_NAME --account-key $ARM_ACCESS_KEY

echo "storage_account_name: $STORAGE_ACCOUNT_NAME"
echo "container_name: $CONTAINER_NAME"
echo "access_key: $ARM_ACCESS_KEY"