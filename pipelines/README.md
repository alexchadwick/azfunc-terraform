AzureDevops pipelines for Demo Azure Function Deployment Project
=========
YAML pipelines which:
  - Use terraform to ensure required azure resources are deployed
    - Will check for any deletion/updates to resources and fail if the parameter FailTerraformDestruct == true
  - Stores globally unique Azure resource names in Azure App Configure
  - Deploys code to Azure Function

Used to test and demo Terraform in AzureDevops pipeline deployment workflow.

Quick start in Azure Devops
----------------
  - Create a Service Connection for Azure Resource Manager
    - Grant resultant AzureAD App Registration/Service Principal appropriate IAM permission for relevant Azure Subscription
      - Storage Account backend resource: IAM role "Storage Blob Data Contributor/Owner"
  - Add the [Azure App Configuration Tasks](https://marketplace.visualstudio.com/publishers/AzureAppConfiguration) to Azure Devops Pipelines
    - These tasks need to be added to the AzDevops Organisation the first time they are used
  - Create an Azure App Configuration instance. Used to store inter-pipeline variables which are created by pipelines
    - E.g. Terraform creates resources with dynamic names, which are referenced in cd
    - See `bash/new-Az-App-Configuration.sh`
  - Asign App Configuration IAM role "App Configuration Data Owner" to the Service Connection Service Principal
  - Create and run new pipelines from existing YAML, using `/pipelines/`: terraform, ci, and cd

Azure Function Test
----------------
Query the Azure Function API HttpTrigger1 e.g.

```curl -X GET https://python-test-functions-cfa655c8705b7a09.azurewebsites.net/api/HttpTrigger1?name=Azure```

Successful reponse is: `Hello, Azure. This HTTP triggered function executed successfully.`

## Self-service terraform using az pipeline parameter input

Aim: Enable pipeline to deploy resources defined in tf config to a service connection which is not statically defined in the pipeline vars file. 
  - Usecase: self-service/adhoc deployment to a users azure subscription for temporary testing
  - Include terraform vars which we may wish to define at runtime

Example `Input` parameter value:
```
appConfigurationKey: 'azfunc-name'
serviceConnection: 'Parameter Test'
tfStageVarFile: '$(System.DefaultWorkingDirectory)/terraform/app/demo-function/env/dev.tfvars'
tfVersion: '0.14.11'
workingDir: '$(System.DefaultWorkingDirectory)/terraform/app/demo-function'
storageBackend:
  backendAzureRmResourceGroupName: 'achdwick-tfstate-storage-rg'
  backendAzureRmStorageAccountName: 'actfstatestorage6597'
  backendAzureRmContainerName: 'tfstate'
  backendAzureRmKey: 'terraform.tfstate'
```

TODO
----------------
- Add Functional test of Azure Function to pipeline. Run test query shown above