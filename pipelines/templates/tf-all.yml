parameters:
  appConfigurationKey: '' # Maybe support multiple az app config key values
  backendAzureRmStorageAccountName: ''
  backendAzureRmContainerName: ''
  backendAzureRmKey: ''
  backendAzureRmResourceGroupName: ''
  serviceConnection: ''
  tfVersion: ''
  tfProvider: 'azurerm'
  tfStageVarFile: ''
  workingDir: ''

stages:
- stage: tf_actions
  jobs:
  - job: tf_actions
    displayName: ${{ parameters.serviceConnection }} terraform actions
    steps:
      - script: |
          echo ${{ parameters.serviceConnection }}
          echo ${{ parameters.tfVersion }}
        displayName: Check values
      - task: TerraformInstaller@0
        displayName: "Install Terraform ${{ parameters.tfVersion }}"
        inputs:
          terraformVersion: '${{ parameters.tfVersion }} '      
      - task: AzureCLI@2
        displayName: Terraform credentials
        inputs:
          azureSubscription: '$(azure_service_connection)'
          scriptType: bash
          scriptLocation: inlineScript
          inlineScript: |
            set -eu  # fail on error
            subscriptionId=$(az account show --query id -o tsv)
            echo "##vso[task.setvariable variable=ARM_CLIENT_ID]$servicePrincipalId"
            echo "##vso[task.setvariable variable=ARM_CLIENT_SECRET;issecret=true]$servicePrincipalKey"
            echo "##vso[task.setvariable variable=ARM_SUBSCRIPTION_ID]$subscriptionId"
            echo "##vso[task.setvariable variable=ARM_TENANT_ID]$tenantId"
          addSpnToEnvironment: true
      - task: AzureCLI@2
        displayName: Terraform init
        inputs:
          azureSubscription: '$(azure_service_connection)'
          scriptType: bash
          scriptLocation: inlineScript
          inlineScript: |
            set -eux  # fail on error
            subscriptionId=$(az account show --query id -o tsv)
            terraform init \
              -backend-config=storage_account_name=actfstatestorage6597 \
              -backend-config=container_name='tfstate' \
              -backend-config=key='terraform.tfstate' \
              -backend-config=resource_group_name=achdwick-tfstate-storage-rg \
              -backend-config=subscription_id="$(ARM_SUBSCRIPTION_ID)" \
              -backend-config=tenant_id="$(ARM_TENANT_ID)" \
              -backend-config=client_id="$(ARM_CLIENT_ID)" \
              -backend-config=client_secret="$(ARM_CLIENT_SECRET)"
          workingDirectory: ${{ parameters.workingDir }}
          addSpnToEnvironment: true
      - bash: |
          terraform plan -input=false -var-file="$TF_VAR_FILE" -out="$OUTPUT_FILE" -detailed-exitcode
        displayName: Terraform Plan
        workingDirectory: ${{ parameters.workingDir }}
        failOnStderr: false
        env:
          OUTPUT_FILE: '$(System.DefaultWorkingDirectory)/pipeline.tfplan'
          TF_VAR_FILE: '${{ parameters.tfStageVarFile }}'
          ARM_SUBSCRIPTION_ID: $(ARM_SUBSCRIPTION_ID)
          ARM_TENANT_ID: $(ARM_TENANT_ID)
          ARM_CLIENT_ID: $(ARM_CLIENT_ID)
          ARM_CLIENT_SECRET: $(ARM_CLIENT_SECRET)
      #Extract Terraform plan out file from Terraform plan above. Fail plan deletes or updates existing resource when pipeline param FailTerraformDestruct == true
      #Where Terraform actions can be: create, read, update, delete, no-op (no-operation) https://www.terraform.io/docs/cloud/sentinel/import/tfplan-v2.html#actions-1
      - bash: |
          #False -> false, so bash will evaluate as bool
          FailTerraformDestruct="${FailTerraformDestruct,,}"
          #Create tfplan json 
          terraform show -json '$(System.DefaultWorkingDirectory)/pipeline.tfplan' >> $jsonPlanFilePath
          ACTIONS=$(jq '.resource_changes[].change | select ((.actions[] == "delete") or (.actions[] == "update")) | {actions: .actions, id: .before.id}' $jsonPlanFilePath)
          if [[ $ACTIONS ]] && $FailTerraformDestruct
          then 
            echo "$ACTIONS" | jq .
            exit 1
          fi
          echo "##vso[task.setvariable variable=FunctionName]$ACTIONS"
        displayName: Terraform Plan Validation
        failOnStderr: true
        workingDirectory: ${{ parameters.workingDir }}
        env:
          TF_VAR_FILE: '$(System.DefaultWorkingDirectory)/pipeline.tfplan'
          jsonPlanFilePath: '$(System.DefaultWorkingDirectory)/tfplan.json'
          FailTerraformDestruct: "${{ parameters.FailTerraformDestruct }}"
      - bash: |
          terraform apply -input=false -auto-approve "$(System.DefaultWorkingDirectory)/pipeline.tfplan"
        displayName: Terraform Apply
        workingDirectory: ${{ parameters.workingDir }}
        env:
          ARM_SUBSCRIPTION_ID: $(ARM_SUBSCRIPTION_ID)
          ARM_TENANT_ID: $(ARM_TENANT_ID)
          ARM_CLIENT_ID: $(ARM_CLIENT_ID)
          ARM_CLIENT_SECRET: $(ARM_CLIENT_SECRET)
      #Build json of key:values from all tf outputs. Slurp results of for loop into one file for import later
      - bash: |
          TF_OUTPUT=$(terraform output -json)
          for OUTPUT in $(echo $TF_OUTPUT| jq -r '.| keys[]')
          do
              echo {\"$OUTPUT\":\"VALUE=$(echo $TF_OUTPUT| jq -r .$OUTPUT.value)\"}
          done | jq --slurp 'reduce .[] as $item ({}; . * $item)' >> $OUTPUT_FILE
          cat $OUTPUT_FILE
        displayName: Terraform output
        workingDirectory: ${{ parameters.workingDir }}
        env:
          OUTPUT_FILE: '$(System.DefaultWorkingDirectory)/import.json'
          ARM_SUBSCRIPTION_ID: $(ARM_SUBSCRIPTION_ID)
          ARM_TENANT_ID: $(ARM_TENANT_ID)
          ARM_CLIENT_ID: $(ARM_CLIENT_ID)
          ARM_CLIENT_SECRET: $(ARM_CLIENT_SECRET)
      - task: AzureAppConfigurationPush@1
        displayName: Push Azure Function Name to App Configuration
        inputs:
          azureSubscription: '${{ parameters.serviceConnection }}'
          ConfigstoreName: '${{ parameters.appConfigurationKey }}'
          ConfigurationFile: '$(System.DefaultWorkingDirectory)/import.json'
          Separator: ':'
          Strict: false
