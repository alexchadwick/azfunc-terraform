resource "azurerm_resource_group" "import" {
    location = "uksouth"
    name     = "achdwick-import-rg"
#    tags     = {}
}

resource "azurerm_app_service_plan" "import" {
    kind                         = "linux"
    location                     = "uksouth"
    name                         = "ac-import-31147-plan"
    reserved                     = true
    resource_group_name          = azurerm_resource_group.import.name

    sku {
        capacity = 1
        size     = "F1"
        tier     = "Free"
    }
#    tags                         = {}
}

resource "azurerm_app_service" "import" {
    app_service_plan_id               = azurerm_app_service_plan.import.id
    client_affinity_enabled           = true
    https_only                        = true
    location                          = "uksouth"
    name                              = "ac-import-31147"
    resource_group_name               = azurerm_resource_group.import.name
    logs {
        detailed_error_messages_enabled = false
        failed_request_tracing_enabled  = false

        application_logs {
            file_system_level = "Off"
        }

        http_logs {
            file_system {
                retention_in_days = 3
                retention_in_mb   = 100
            }
        }
    }
    site_config {
        always_on                   = false
        default_documents           = [
            "Default.htm",
            "Default.html",
            "Default.asp",
            "index.htm",
            "index.html",
            "iisstart.htm",
            "default.aspx",
            "index.php",
            "hostingstart.html",
        ]
        dotnet_framework_version    = "v4.0"
        ftps_state                  = "AllAllowed"
        http2_enabled               = true
        ip_restriction              = []
        linux_fx_version            = "PYTHON|3.7"
        local_mysql_enabled         = false
        managed_pipeline_mode       = "Integrated"
        min_tls_version             = "1.2"
        number_of_workers           = 1
        remote_debugging_enabled    = false
        scm_ip_restriction          = []
        scm_type                    = "None"
        scm_use_main_ip_restriction = false
        use_32_bit_worker_process   = true
        websockets_enabled          = false
    }
#    tags                              = {}
}