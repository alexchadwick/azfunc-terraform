#Not used by pipeline, test if need to add to gitignore
terraform {
  backend "azurerm" {}
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
  required_version = ">= 0.13"
}

# Need to add this for "features {}", which is required for azurerm provider v2+
# See this issue: https://github.com/terraform-providers/terraform-provider-azurerm/issues/7359
provider "azurerm" {
  features {}
}