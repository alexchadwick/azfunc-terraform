resource "azurerm_resource_group" "this" {
  name     = var.project_name
  location = var.location
}

data "azurerm_app_service_plan" "this" {
  name                = var.app_service_plan_config.name
  resource_group_name = var.app_service_plan_config.resource_group_name
}

resource "azurerm_application_insights" "this" {
  name                = var.project_name
  location            = var.location
  resource_group_name = azurerm_resource_group.this.name
  application_type    = "other"
  retention_in_days   = 90
}

resource "azurerm_app_service" "this" {
  name                = var.project_name
  location            = var.location
  resource_group_name = azurerm_resource_group.this.name
  app_service_plan_id = data.azurerm_app_service_plan.this.id

  app_settings = {
    "APPINSIGHTS_INSTRUMENTATIONKEY" = "${azurerm_application_insights.this.instrumentation_key}"
  }
}