stage = "dev"
location = "uksouth"
resource_group_name = "ms-sql"
resource_group_cannot_delete = 1
sql_server_read_only = 1

#short_term_retention_policy equates to the pitr backups
ms_sql_server = {
    "name" = "ac-ms-sql",
    "administrator_login" = "achadwick",
    "version" = "12.0",
    "public_network_access_enabled" = true,
    "max_size_gb" = 250
}

ms_sql_databases = {
    "default" = {     
        pitr_days = 35,
        max_size_gb = 250,
        sku_name = "S0",
        storage_account_type = "GRS",
        monthly_retention = "P1Y", #Retain monthly backups for 1 year
        yearly_retention = "P5Y", #Retain yearly backups for 5 years
        week_of_year = 25
    },
    "shortTermBackupOnly" = {       
        pitr_days = 7,
        max_size_gb = 2,
        sku_name = "Basic",
        storage_account_type = "GRS"
    }
}

firewall_cidr = {
    "Me" = {
        end_ip_address   = "188.210.214.37"
        start_ip_address = "188.210.214.37"
    },
    "AllowAllWindowsAzureIps" = {
        end_ip_address   = "0.0.0.0"
        start_ip_address = "0.0.0.0"
    }
}