#Not used by pipeline, test if need to add to gitignore
terraform {
  backend "azurerm" {}
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
  required_version = ">= 0.13"
}