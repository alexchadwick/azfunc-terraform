#Lifted from Terraform registry example here: https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/function_app#example-usage-in-a-consumption-plan
#Uses random_id for resources which require globally unique names

resource "random_id" "id" {
	  byte_length = 8
}

resource "azurerm_resource_group" "group1" {
  name = "group1${random_id.id.hex}"
  location = var.location
}

resource "azurerm_resource_group" "group2" {
  name = "group2${random_id.id.hex}"
  location = var.location
}

resource "azurerm_resource_group" "group3" {
  name = "group3${random_id.id.hex}"
  location = var.location
}