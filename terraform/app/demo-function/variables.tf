variable "group_name" {
  type = string
}

variable "location" {
  type    = string
  default = "uksouth"
}

variable "stage" {
  type    = string
  default = "dev"
}