Terraform config for Demo Azure Function Deployment Project
=========
Provisions Azure resources required for single Azure Function (Consumption plan). 

Used to test and demo Terraform in AzureDevops pipeline deployment workflow.

Consumes Terraform module `azure-function`, using environmental specific stage variables (e.g. `terraform/app/demo-function/env/dev.tfvars`) and Terraform backends (e.g. `terraform/app/demo-function/env/backend-dev`)

# Quick start on Linux

  - Install terraform cli: https://learn.hashicorp.com/tutorials/terraform/install-cli
  - Install Azure CLI and sign in: https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-linux
    - Confirm your default azure subscription is correct
    ```
    az account list --query [?isDefault]
    az account set --subscription "<name_of_subscription>
    ```
  - Create ARM backend: Azure Blob Storage account Container. See `bash/new-ARM-backend.sh`
    - Note script output: Az Storage Account Name and Access Key created for terraform backend storage
        - Update contents of `terraform/app/demo-function/env/backend-dev`
        - Ensure env var `$ARM_ACCESS_KEY` is set
  - Deploy infra with terraform

# Terraform cmds
Using the cli on a workstation to delpoy config using dev tfvars, backend files and a secret tfvars file which is not commited to the repo:

Working dir == `/terraform/<service-name>` 
```
terraform fmt
terraform init -backend-config env/backend-dev
terraform validate
terraform plan -var-file env/dev.tfvars -out dev.tfplan -var-file env/dev.secret.tfvars
terraform apply ./dev.tfplan
terraform destroy -var-file env/dev.tfvars -var-file env/dev.secret.tfvars
```
# Generate Terraform module READMEs

Uses [terraform-docs](https://terraform-docs.io/). Add extra README content to a comment in `main.tf`

Working dir == `/terraform/modules/<module-name>` 
```
docker run --rm -v $(pwd):/data quay.io/terraform-docs/terraform-docs:0.14.1 md ./data > README.md
```