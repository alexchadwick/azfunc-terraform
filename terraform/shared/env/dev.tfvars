stage = "dev"
location = "uksouth"
project_name = "homelab"
app_service_plan_config = {
    kind = "Windows",
    tier = "Basic",
    size = "B1"
}

owners = [
    "alex.j.chadwick_gmail.com#EXT#@alexjchadwickgmail.onmicrosoft.com",
    "owner@alexjchadwickgmail.onmicrosoft.com"
]

readers = [
    "reader@alexjchadwickgmail.onmicrosoft.com"
]