variable "project_name" {
  type = string
}

variable "location" {
  type    = string
  default = "uksouth"
}

variable "stage" {
  type = string
}

variable "app_service_plan_config" {
  type = map(any)
  default = {
    kind = "Windows",
    tier = "Basic",
    size = "B1"
  }

  validation {
    condition     = contains(["FunctionApp", "Linux", "Windows", "elastic"], var.app_service_plan_config.kind)
    error_message = "Valid values for app_service_plan_config.kind are 'FunctionApp' (Consumption Plan), 'elastic' (Premium Consumption), 'Windows' or 'Linux' (Dedcated/Shared compute)."
  }
  validation {
    condition     = contains(["Dynamic", "Basic", "Standard", "PremiumV2", "PremiumV3", "Free", "Shared"], var.app_service_plan_config.tier)
    error_message = "Valid values for app_service_plan_config.tier are 'Dynamic' (Consumption Plan), 'Standard', 'Basic', 'PremiumV2', 'PremiumV3' (Dedcated compute) or 'Free', 'Shared' (Shared compute)."
  }
}

variable "owners" {
  type    = list(string)
  default = null
}

variable "readers" {
  type    = list(string)
  default = null
}