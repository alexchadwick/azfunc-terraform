resource "azurerm_resource_group" "this" {
  name     = "${var.project_name}_shared"
  location = var.location
}

# Limitations: https://docs.microsoft.com/en-us/azure/app-service/overview#limitations
resource "azurerm_app_service_plan" "this" {
  name                = "${var.project_name}_shared"
  location            = var.location
  resource_group_name = azurerm_resource_group.this.name
  kind                = var.app_service_plan_config.kind
  reserved            = var.app_service_plan_config.kind == "Linux" ? true : false

  sku {
    tier = var.app_service_plan_config.tier
    size = var.app_service_plan_config.size
  }
}