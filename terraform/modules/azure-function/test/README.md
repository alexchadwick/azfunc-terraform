# Terratest

## Quick start
- [Install Golang](https://golang.org/doc/install)
- See [Terragrunt Quick Start](https://terratest.gruntwork.io/docs/getting-started/quick-start/)
- See [Terragrunt godocs](https://pkg.go.dev/github.com/gruntwork-io/terratest?utm_source=godoc)

Working dir == `/terraform/modules/azure-function/test` 
```
go mod tidy 
go test -v [-run TestLinuxFunctionAppDefault] # Optionally run specific tests
```